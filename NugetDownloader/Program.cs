﻿using NuGet.Common;
using NuGet.Packaging;
using NuGet.Packaging.Core;
using NuGet.Protocol;
using NuGet.Protocol.Core.Types;
using NuGet.Versioning;
using System;
using System.Configuration;
using System.IO;
using System.Threading;

namespace DownloadNugetDemo
{
    class Program
    {
        static async System.Threading.Tasks.Task Main(string[] args)
        {
            ILogger logger = NullLogger.Instance;
            CancellationToken cancellationToken = CancellationToken.None;

            SourceCacheContext cache = new SourceCacheContext();
            SourceRepository repository = Repository.Factory.GetCoreV3(ConfigurationManager.AppSettings["NugetUrl"]);
            FindPackageByIdResource resource = await repository.GetResourceAsync<FindPackageByIdResource>();

            string packageId = ConfigurationManager.AppSettings["PackageId"];
            NuGetVersion packageVersion = new NuGetVersion(ConfigurationManager.AppSettings["PackageVersion"]);
            using MemoryStream packageStream = new MemoryStream();
            await resource.CopyNupkgToStreamAsync(
                packageId,
                packageVersion,
                packageStream,
                cache,
                logger,
                cancellationToken);

            {
                var pkgDownloadContext = new PackageDownloadContext(cache);
                var downloadRes = await repository.GetResourceAsync<DownloadResource>();

                var downloadResult = await downloadRes.GetDownloadResourceResultAsync(
                        new PackageIdentity(packageId, packageVersion),
                        pkgDownloadContext,
                        @"packages",
                        logger,
                        CancellationToken.None);

                Console.WriteLine(downloadResult.Status.ToString());
            }

            Console.WriteLine($"Downloaded package {packageId} {packageVersion}");

            using PackageArchiveReader packageReader = new PackageArchiveReader(packageStream);
            NuspecReader nuspecReader = await packageReader.GetNuspecReaderAsync(cancellationToken);

            Console.WriteLine($"Tags: {nuspecReader.GetTags()}");
            Console.WriteLine($"Description: {nuspecReader.GetDescription()}");
            Console.ReadKey();
        }
    }
}
